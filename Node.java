import java.util.ArrayList;
import java.util.List;

public class Node{
	private ArrayList<Map> maps;
	private Node parent;
	private ArrayList<Node> children;
	private Node prev;
	private Node next;
	
	/** Getter and Setter Methods for the above attributes
	 * @return
	 */
	public List<Map> getMaps() {
		return maps;
	}
	public void setMaps(ArrayList<Map> arrayList) {
		this.maps = arrayList;
	}
	public Node getParent() {
		return parent;
	}
	public void setParent(Node parent) {
		this.parent = parent;
	}
	public List<Node> getChildren() {
		return children;
	}
	public void setChildren(List<Node> children) {
		this.children = (ArrayList<Node>) children;
	}
	public Node getPrev() {
		return prev;
	}
	public void setPrev(Node prev) {
		this.prev = prev;
	}
	public Node getNext() {
		return next;
	}
	public void setNext(Node next) {
		this.next = next;
	}
	
	
	/**
	 * Constructors for the class
	 */
	public Node() {
		this.maps = new ArrayList<Map>();
		this.children = new ArrayList<Node>();
		this.prev = null;
		this.next = null;
		this.parent = null;
	}
	
	public Node(int midKey) {
		this.maps = new ArrayList<Map>();
		this.children = new ArrayList<Node>();
		this.prev = this.next = null;
		this.parent = null;
		this.maps.add(new Map(midKey));
		
	}
	
	/**
	 * Function to get the child index in the node. 
	 * Searches the maps in the node to find the correct index of the the child to look for.
	 * @param key - search key - the number we wish to find the index
	 * @return - index in the node
	 * 			returns maps.size() if key is greater than all elements
	 */
	public int findAppropriateIndex(int key) {
		int i = 0;
		for(i=0; i<this.maps.size(); i++) {
			if(key < this.maps.get(i).getKey()) {
				break;
			}
		}
		return i;
	}
	
	/**
	 * Function to get the exact index in a leaf node. This is used to get exact index of key rather than the likely position.
	 * @param key
	 * @return - returns -1 if the key is not found in the list of maps.
	 */
	public int findIndexInLeaf(int key) {
		if(this.maps.size() == 0 || key < this.maps.get(0).getKey() || key > this.maps.get(this.maps.size() - 1).getKey()) {
			return -1;
		}else {
			int i;
			for(i=0; i<this.maps.size(); i++) {
				if(key <= this.maps.get(i).getKey()) {
					return i;
				}
			}
		}
		return -1;
	}
		
	@Override
	public String toString() {
		String t = "";
		for(Map m: this.maps) {
			t += m.toString();
		}
		return "Node [maps=" + t + "]";
	}

	/** 
	 * Helper methods to assist the main program
	 * @return
	 */
	public boolean isLeaf() {
		if(this.children.size() == 0) {
			return true;
		}
		return false;
	}
	
	public Node getLeftInternalSibling() {
		Node parentNode = this.parent, prevChild = null;
		for(Node child: parentNode.getChildren()) {
			if(child.equals(this)) {
				break;
			}else
				prevChild = child;
		}
		return prevChild;
	}
	public Node getRightInternalSibling() {
		Node parentNode = this.parent, nextChild = null;
		for(Node child: parentNode.getChildren()) {
			if(child.equals(this)) {
				break;
			}else
				nextChild = child;
		}
		return nextChild;
	}
	
	public void addMap(int index, Map map) {
		if(index == -1) {
			this.maps.add(map);
		}else {
			this.maps.add(index, map);
		}
	}
	public void addChild(int index, Node cur) {
		if(index == -1) {
			this.children.add(cur);
		}else {
			this.children.add(index, cur);
		}
	}
	public void deleteMapAtIndex(int indexInLeaf) {
		this.maps.remove(indexInLeaf);
	}
	public void setMap(int i, int key) {
		this.maps.get(i).setKey(key);
	}
	public void deleteMap(int index) {
		this.maps.remove(index);
	}
	public void deleteChild(int index) {
		this.children.remove(index);
	}
	public Map getLastMap() {
		Map x = this.maps.get(this.maps.size() - 1);
		this.maps.remove(this.maps.size() - 1);
		return x;
	}
	public Node getLastChild() {
		Node x = this.children.get(this.children.size() - 1);
		this.children.remove(this.children.size() - 1);
		return x;
	}
	public Map getFirstMap() {
		Map x = this.maps.get(0);
		this.maps.remove(0);
		return x;
	}
	public Node getFirstChild() {
		Node x = this.children.get(0);
		this.children.remove(0);
		return x;
	}

}
