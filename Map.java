
public class Map {
	private int key;
	private double value;
	
	public Map(int key, double value) {
		this.key = key;
		this.value = value;
	}
	public Map(int key) {
		this.key = key;
	}
	
	public Map(Map m) {
		// TODO Auto-generated constructor stub
		this.key = m.getKey();
		this.value = m.getValue();
	}
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "key=" + key + " ";
	}
	
	
}
