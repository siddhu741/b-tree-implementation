import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author siddu
 *
 */
public class BPlusTree {
	private Node root;
	private int degree;

	public Node getRoot() {
		return root;
	}
	public void setRoot(Node root) {
		this.root = root;
		this.root.setParent(null);
	}

	/**
	 * Constructor method
	 * @param degree
	 */
	public BPlusTree(int degree) {
		this.degree = degree;
		this.root = new Node();
	}

	/**
	 * Method to check if the node is overflow after inserting a new node
	 * @param n
	 * @return
	 */
	public boolean isOverflow(Node n) {
		if (n.getMaps().size() >= this.degree)
			return true;
		return false;
	}

	/**
	 * This method iterates down the tree to find a leaf node to insert. After
	 * insertion checks if node is overflowing and then splits the node to two
	 * halves using splitExternalNode method in Node.java Finally, calls
	 * insertInIngernalNode method on the parent. Steps
	 * 1. initially finds the leaf node to insert
	 * 2. insert at correct location in leaf
	 * 3. check for Overflow after insertion.
	 * 4. if(overflow) split the node to two halves by calling splitExternalNode() method
	 * 5. if the cur node is not the root, insert midKey() into parent by calling  insertInInternalNode().
	 * 		5.1 else adjust the pointers
	 * 
	 * @param key   - key which is to be inserted
	 * @param value - value associated with the key
	 */
	public void insert(int key, double value) {
		Node cur = this.root;
		while (!cur.isLeaf()) {
			int index = cur.findAppropriateIndex(key);
			cur = cur.getChildren().get(index);
		}
		int indexAtLeaf = cur.findAppropriateIndex(key);
		cur.addMap(indexAtLeaf, new Map(key, value));

		if (isOverflow(cur)) {
			// handle overflow for leaf
			int midKey = cur.getMaps().get(this.degree / 2).getKey();
			Node leafRight = splitExternalNode(cur);
			leafRight.setNext(cur.getNext());
			if (cur.getNext() != null) {
				cur.getNext().setPrev(leafRight);
			}
			cur.setNext(leafRight);
			leafRight.setPrev(cur);

			if (this.root == cur) {
				Node newMid = new Node(midKey);
				this.root = newMid;
				newMid.addChild(0, cur);
				newMid.addChild(1, leafRight);
				cur.setParent(newMid);
				leafRight.setParent(newMid);
			} else {
				// add midkey to cur's parent and check for overflow
				insertInInternalNode(midKey, cur.getParent(), leafRight);

			}
		}
	}
	
	/**
	 * Splits an external node into two equal halves.
	 * make sure to check for overflow before calling this method
	 * @param m
	 * @return - right child after split
	 */
	private Node splitExternalNode(Node cur) {
		Node right = new Node();
		List<Map> secondHalf = cur.getMaps().subList(this.degree/2, cur.getMaps().size());
		right.setMaps(new ArrayList<Map>(secondHalf));
		cur.setMaps(new ArrayList<Map>(cur.getMaps().subList(0, (this.degree/2))));
		return right;
	}
	
	/**
	 * Split the internal node to handle overflow in InternalNode.
	 * Creates a new Node and splits the maps  and children with itself and the newly created Node.
	 * @param m
	 * @return - right child after the split
	 */
	private Node splitInternalNode(Node cur) {
		Node right = new Node();
		right.setMaps(new ArrayList<Map>(cur.getMaps().subList((this.degree/2)+1, cur.getMaps().size())));
		cur.setMaps(new ArrayList<Map>(cur.getMaps().subList(0, (this.degree/2))));
		
		right.setChildren(new ArrayList<Node>(cur.getChildren().subList(this.degree/2+1, cur.getChildren().size())));
		cur.setChildren(new ArrayList<Node>(cur.getChildren().subList(0, this.degree/2+1)));
		
		for(Node child: right.getChildren()) {
			child.setParent(right);
		}
		return right;
	}
	
	
	/**
	 * this function inserts the given key into an internal node.
	 * 1. insert the given key and right subtree into the node
	 * 2. check for overflow
	 * 3. if overflow
	 * 	3.1 split the node into two halves by calling splitInternalNode()
	 *  3.2 check if the node is root
	 *  	3.2.1 if root, then adjust pointers 
	 *  	3.2.2 else, recursively insert midKey and the new right subtree into its parent.
	 * @param key - key which is to be inserted in internal Node. It is the result of the split of a Node in previous flow.
	 * @param node - Node where the key has to be inserted.
	 * @param right - the Right subtree to insert.
	 */
	public void insertInInternalNode(int key, Node node, Node right) {
		int index = node.findAppropriateIndex(key);
		node.addMap(index, new Map(key));
		node.addChild(index + 1, right);
		right.setParent(node);
		if (isOverflow(node)) {
			int midKey = node.getMaps().get(this.degree / 2).getKey();
			Node iRight = splitInternalNode(node);
			if (this.root == node) {
				Node newMid = new Node(midKey);
				this.root = newMid;
				newMid.addChild(0, node);
				newMid.addChild(1, iRight);

				node.setParent(newMid);
				iRight.setParent(newMid);
			} else {
				insertInInternalNode(midKey, node.getParent(), iRight);
			}

		}
	}

	/**
	 * Method to search the BPlusTree 
	 * Starts from the root and traverses down to get the leaf by getting the appropriate child at each level.
	 * after getting to the leaf traverses in the mapsList to match for the given key 
	 * 	if key is in the  mapsList returns the corresponding value.
	 * 		else returns "Null"
	 * @param key
	 * @return - "Null" if  not found and prints the value of the Map if found
	 */
	public String search(int key) {
		Node cur = this.root;
		while (!cur.isLeaf()) {
			cur = cur.getChildren().get(cur.findAppropriateIndex(key));
		}
		for(Map x: cur.getMaps()) {
			if(x.getKey() == key) {
				return Double.toString(x.getValue());
			}
		}
		return "Null";
	}

	/**
	 * Method to get the range search queries.
	 * 1. Starts from the root and traverses down to get the leaf, by getting the appropriate child at each level.
	 * 2. after finding the leaf to find the 'key'(minimum key). it traverses all the leaves to the right.
	 * 3. while traversing sideways, it appends all the values whose key falls between 'key' and 'maxKey'
	 * 3. the sideward search terminates once we find a map where the map's key is greater that of given maxKey  
	 * 
	 * 	if key is in the  mapsList returns the corresponding value.
	 * 		else returns "Null"
	 * @param key
	 * @param maxKey
	 * @return
	 */
	public String search(int key, int maxKey) {
		Node cur = this.root;
		List<Double> out = new ArrayList<Double>();
		boolean foundMax = false;
		while (!cur.isLeaf()) {
			cur = cur.getChildren().get(cur.findAppropriateIndex(key));
		}
		while(cur != null && !foundMax) {
			for (Map x : cur.getMaps()) {
				if (x.getKey() >= key && x.getKey() <= maxKey) {
					out.add(x.getValue());
				}else if(x.getKey() > maxKey) {
					foundMax = true;
					break;
				}
			}
			cur = cur.getNext();
		}
		if (out.size() == 0) {
			return "Null";
		}
		String output = "";
		for(Double d: out) {
			if(output.equals("")) {
				output += Double.toString(d) + ",";
			}else output += Double.toString(d);
		}
		return output;
	}

	
	/**
	 * Helper methods to find if the nodes are underflow 
	 * @param n
	 * @return
	 */
	public boolean isUnderflow(Node n) {
		if (n.getMaps().size() < (this.degree) / 2)
			return true;
		return false;
	}

	public boolean isUnderflowAfterDelete(Node n) {
		if (n.getMaps().size() - 1 < (this.degree) / 2)
			return true;
		return false;
	}

	/**
	 * Method to delete the given key- value pair from the Tree.
	 * 1. performs a search for the given key and returns if not found.
	 * 2. if found, it deletes the Map(key value pair) from the leaf.
	 * 3. After deleting, checks if the node is underFlown 
	 * 	3.1 if underflowed, it calls the balanceLeafNode() 
	 * @param key - key to delete from BTree
	 */
	public void delete(int key) {
		Node cur = this.root;
		while (!cur.isLeaf()) {
			int appropriateIndexInParent = cur.findAppropriateIndex(key);
			cur = cur.getChildren().get(appropriateIndexInParent);
		}
		int indexInLeaf = cur.findIndexInLeaf(key);
		if (indexInLeaf != -1) {
			cur.deleteMapAtIndex(indexInLeaf);
		}
		if (cur != this.root && isUnderflow(cur)) {
			deleteInLeafNode(cur, key);
		}
	}

	/**
	 * 1. Gets the left and right subtrees
	 * 2. if we can borrow from the left or right sibling, calls borrow methods
	 * 3. if we cannot borrow from siblings, it merges the node with its sibling and parent by calling merge methods.
	 * 	3.1 After merging, calls the method to delete the parent of the surviving sibling.
	 * @param cur - Node which needs to be balanced
	 * @param key - the key-value which is deleted(useful for the parent index computations)
	 */
	private void deleteInLeafNode(Node cur, int key) {
		Node parent = cur.getParent(), sibling = null, leftSibling = cur.getPrev(), rightSibling = cur.getNext();
		if(leftSibling == null || !leftSibling.getParent().equals(parent)) {
			leftSibling = null;
		}
		if(rightSibling == null || !rightSibling.getParent().equals(parent)) {
			rightSibling = null;
		}
		if(leftSibling != null && !isUnderflowAfterDelete(leftSibling)) {
			borrowLastFromLeftSibling(leftSibling, cur, parent, key);
		}else if (rightSibling != null && !isUnderflowAfterDelete(rightSibling)) {
			borrowFirstFromRightSibling(rightSibling, cur, parent, key);
		}else{
			if(rightSibling != null) {
				mergeWithRightLeaf(cur, rightSibling, key);
				sibling = rightSibling;
			}else{
				mergeWithLeftLeaf(cur, leftSibling, key);
				sibling = leftSibling;
			}
			if (isUnderflow(sibling.getParent())) {
				internalDelete(sibling.getParent(), key);
			}

		}

	}

	/**
	 * 
	 * Adds all the maps from the 'cur' node to its sibling
	 * Deletes the parents' map and the corresponding child 
	 * Updates the pointers of sibling
	 * Sets the 'cur' node to null for garbage collector to clean it up
	 * @param cur - Node which is to be merged with the sibling
	 * @param rightSibling - Node to dump the surviving maps of 'cur'
	 * @param key - initial key(for  deletion) which is used to get the indexOfParent easily
	 */
	private void mergeWithRightLeaf(Node cur, Node rightSibling, int key) {
		Node parent = cur.getParent();
		int indexParent = parent.findAppropriateIndex(key);
		Collections.reverse(cur.getMaps());
		for (Map m : cur.getMaps()) {
			rightSibling.addMap(0, m);
		}
		parent.deleteMap(indexParent);
		parent.deleteChild(indexParent);
		rightSibling.setPrev(cur.getPrev());
		if(cur.getPrev() != null) {
			cur.getPrev().setNext(rightSibling);
		}
		cur = null;
	}
	
	/**
	 * Adds all the maps from the 'cur' node to its sibling
	 * Deletes the parents' map and the corresponding child 
	 * Updates the pointers of sibling
	 * Sets the 'cur' node to null for garbage collector to clean it up
	 * @param cur - Node which is to be merged with the sibling
	 * @param leftSibling - Node to dump the surviving maps of 'cur'
	 * @param key - initial key(for  deletion) which is used to get the indexOfParent easily
	 */
	private void mergeWithLeftLeaf(Node cur, Node leftSibling, int key) {
		Node parent = cur.getParent();
		int indexParent = parent.findAppropriateIndex(key);
		for (Map m : cur.getMaps()) {
			leftSibling.addMap(-1, m);
		}
		leftSibling.setNext(cur.getNext());
		if(cur.getNext() != null) {
			cur.getNext().setPrev(leftSibling);
		}
		parent.deleteMap(indexParent - 1);
		parent.deleteChild(indexParent);
		cur = null;
	}

	/**
	 * 1. Borrows the first element from the right sibling.
	 * 2. Adds the borrowed map to the current node's end.
	 * 3. Updates the corresponding parent node with the key from the (new) first element of rightSibling  
	 * @param rightSibling - sibling from which to borrow
	 * @param cur - current node which is underflowed
	 * @param parent - parent of cur node
	 * @param key - the deleted key
	 */
	private void borrowFirstFromRightSibling(Node rightSibling, Node cur, Node parent, int key) {
		Map siblingFirstEntry = rightSibling.getFirstMap();
		int keyIndex = parent.findAppropriateIndex(key);
		cur.addMap(-1, siblingFirstEntry);
		parent.setMap(keyIndex, rightSibling.getMaps().get(0).getKey());
	}

	/**
	 * 1. Borrows the last map from the left sibling.
	 * 2. Adds the borrowed map to the current node's start.
	 * 3. Updates the corresponding parent node with the key from the (new) first element of rightSibling  
	 * @param leftSibling - sibling from which to borrow
	 * @param cur - current node which is underflowed
	 * @param parent - parent of cur node
	 * @param key - the deleted key
	 */
	private void borrowLastFromLeftSibling(Node leftSibling, Node cur, Node parent, int key) {
		Map siblingLastEntry = leftSibling.getLastMap();
		int keyIndex = parent.findAppropriateIndex(key);
		cur.addMap(0, siblingLastEntry);
		parent.setMap(keyIndex - 1, siblingLastEntry.getKey());
	}

	/**
	 * 1. checks if the internal node is a root and sets root, if empty
	 * 2. computes left and right internal siblings
	 * 3. checks if the given node can borrow from the siblings and calls borrow internal methods
	 * 4. if borrow is not possible calls the merge methods with siblings
	 * 	4.1 after the merge if the siblings' parent is underflowed, call internalDelete recursively with the siblings' parent.
	 * 
	 * @param node - current internal node which is underflowed
	 * @param key - the deleted key-value pair's key - used for index computations
	 */
	public void internalDelete(Node node, int key) {
		if (this.root == node && node.getMaps().size() == 0) {
			setRoot(node.getChildren().get(0));
			return;
		}
		if(this.root == node && node.getMaps().size() + 1 == node.getChildren().size())
			return;
		Node parent = node.getParent();
		int indexInParent = parent.findAppropriateIndex(key);
		Node leftSibling = node.getLeftInternalSibling(), rightSibling = null;
		if (leftSibling == null) {
			if (parent.getChildren().size() > indexInParent + 1) {
				rightSibling = parent.getChildren().get(indexInParent + 1);
			}
		}
		if (leftSibling != null && !isUnderflowAfterDelete(leftSibling)) {
			borrowFromLeftInternalSibling(node, leftSibling, indexInParent, parent);
		} else if (rightSibling != null && !isUnderflowAfterDelete(rightSibling)) {
			borrowFromRightInternalSibling(node, rightSibling, indexInParent, parent);
		} else {
			Node sibling = null;
			if (rightSibling != null) {
				mergeWithRightSiblingAndParentKey(node, parent, rightSibling, key);
				sibling = rightSibling;
			} else {
				mergeWithLefttSiblingAndParentKey(node, parent, leftSibling, key);
				sibling = leftSibling;
			}
			if (isUnderflow(sibling.getParent())) {
				internalDelete(sibling.getParent(), key);
			}
		}

	}

	/**
	 * 1. Adds parent pointer at the end of the node's maps. 
	 * 2. Moves all maps from the current node to its sibling.
	 * 3. Moves the children as well and update children's parent pointer.
	 * 4. Deleted the parent's Map and Child (as it is merged)
	 * 5. sets the 'node' to null for garbage collector to clean it
	 * @param node - current node to be merged with sibling
	 * @param parent - parent of the current node
	 * @param leftSibling - sibling node
	 * @param key - deleted Key
	 */
	private void mergeWithLefttSiblingAndParentKey(Node node, Node parent, Node leftSibling, int key) {
		int indexInParent = parent.findAppropriateIndex(key);
		Map parentMap = parent.getMaps().get(indexInParent - 1);
		node.addMap(-1, parentMap);
		for (Map m : node.getMaps()) {
			leftSibling.addMap(-1, m);
		}
		for (Node child : node.getChildren()) {
			leftSibling.addChild(-1, child);
			child.setParent(leftSibling);
		}
		parent.deleteMap(indexInParent-1);
		parent.deleteChild(indexInParent);
		node = null;
	}
	
	/**
	 * 1. Adds parent pointer at the first of the node's maps. 
	 * 2. Moves all maps from the current node to its sibling(reversing because i need to maintain the same ascending order).
	 * 3. Moves the children as well and update children's parent pointer.
	 * 4. Deleted the parent's Map and Child (as it is merged)
	 * 5. sets the 'node' to null for garbage collector to clean it
	 * @param node - current node to be merged with sibling
	 * @param parent - parent of the current node
	 * @param rightSibling - sibling node
	 * @param key - deleted Key
	 */
	private void mergeWithRightSiblingAndParentKey(Node node, Node parent, Node rightSibling, int key) {
		int indexInParent = parent.findAppropriateIndex(key);
		Map parentMap = parent.getMaps().get(indexInParent);
		rightSibling.addMap(0, parentMap);
		Collections.reverse(node.getMaps());
		for (Map m : node.getMaps()) {
			rightSibling.addMap(0, m);
		}
		Collections.reverse(node.getChildren());
		for (Node child : node.getChildren()) {
			rightSibling.addChild(0, child);
			child.setParent(rightSibling);
		}
		parent.deleteMap(indexInParent);
		parent.deleteChild(indexInParent);
		node = null;
	}

	/**
	 * 1. Adds parentMap at indexInParent-1 position to node's maps
	 * 2. Sets the parent's mao at the above position to leftSibling
	 * 3. Fetches the last map from left sibling  and adds it in the first of node's maps
	 * 4. Adds last child of the left sibling to the Node
	 * 5. Updates the parent pointer of above child node 
	 * @param node - current node which needs to borrow
	 * @param leftSibling - sibling ready to donate
	 * @param indexInParent - appropriate position from parent of the current node
	 * @param parent - parent of the given node.
	 */
	private void borrowFromLeftInternalSibling(Node node, Node leftSibling, int indexInParent, Node parent) {
		Map parentMap = (parent.getMaps().get(indexInParent - 1));
		node.addMap(0, new Map(parentMap));
		Map siblingLast = leftSibling.getLastMap();
		parent.setMap(indexInParent - 1, siblingLast.getKey());
		Node siblingLastChild = leftSibling.getLastChild();
		node.addChild(0, siblingLastChild);
		siblingLastChild.setParent(node);
	}
	
	/**
	 * 1. Adds parentMap at indexInParent position to node's maps
	 * 2. Sets the parent's map at the above position to rightSibling's first map.
	 * 3. Fetches the first map from right sibling  and adds it in the end of node's maps
	 * 4. Adds first child of the right sibling to the Node
	 * 5. Updates the parent pointer of above child node 
	 * 
	 * @param node - current node which needs to borrow
	 * @param leftSibling - sibling ready to donate
	 * @param indexInParent - appropriate position from parent of the current node
	 * @param parent - parent of the given node.
	 */
	private void borrowFromRightInternalSibling(Node cur, Node rightSibling, int indexInParent, Node parent) {
		Map parentMap = parent.getMaps().get(indexInParent);
		cur.addMap(-1, new Map(parentMap));
		Map siblingFirst = rightSibling.getFirstMap();
		parent.setMap(indexInParent - 1, siblingFirst.getKey());
		Node siblingFirstChild = rightSibling.getFirstChild();
		cur.addChild(-1, siblingFirstChild);
		siblingFirstChild.setParent(cur);
	}
	
	@Override
	public String toString() {
		String t = "";
		Queue q = new LinkedList();
		q.add(this.root);
		q.add(null);
		while (!q.isEmpty()) {
			Node cur = (Node) q.poll();
			Node nextCur = (Node) q.peek();
			if (cur == null && nextCur != null) {
				t += "\n\n";
				q.add(null);
			} else {
				if (cur != null) {
					t += cur.toString() + " | ";
					for (Node child : cur.getChildren()) {
						q.add(child);
					}
				}
			}
		}
		return t;
	}
}
