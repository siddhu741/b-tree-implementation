JCC = javac
JFLAGS = -g

default: bplustree.class

bplustree.class: bplustree.java
	$(JCC) $(JFLAGS) bplustree.java
	
Node.class: Node.java
	$(JCC) $(JFLAGS) Node.java
	
BPlusTree.class: BPlusTree.java
	$(JCC) $(JFLAGS) BPlusTree.java
	
clean:
	$(RM) *.class
