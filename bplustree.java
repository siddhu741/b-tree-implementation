import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class bplustree {

	public static void main(String[] args) {

		try {
			String inputFileName = args[0];

			File inputFile = new File(inputFileName);
			FileReader fileReader = new FileReader(inputFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			BufferedWriter bufferedWriter = createNewFile();

			String line = bufferedReader.readLine();
			String[] input = line.split("\\(|,|\\)");
			BPlusTree b = new BPlusTree((Integer.parseInt(input[1])));

			while ((line = bufferedReader.readLine()) != null) {
				input = line.split("\\(|,|\\)");
				switch (input[0].trim()) {
					// for inserting element into B Plus Tree
					case "Insert": {
						b.insert(Integer.parseInt(input[1].trim()), Double.parseDouble(input[2].trim()));
						break;
					}
					case "Search": {
						// for finding all key value pairs between two keys
						if (input.length == 2) {
							String out = b.search(Integer.parseInt(input[1].trim()));
							bufferedWriter.write(out);
							bufferedWriter.newLine();
						} else {
							String out = b.search(Integer.parseInt(input[1].trim()), Integer.parseInt(input[2].trim()));
							bufferedWriter.write(out);
							bufferedWriter.newLine();
						}
						break;
					}
					case "Delete": {
						b.delete(Integer.parseInt(input[1].trim()));
						break;
					}
				}
			}
			fileReader.close();
			
			bufferedWriter.close();
			System.out.println("************************");
			System.out.print(b);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static BufferedWriter createNewFile() throws IOException {
		// Creating a new file to write output to (output_file.txt)
		File file = new File("output_file.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		return bw;
	}

}
